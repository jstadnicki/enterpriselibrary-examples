﻿namespace EL002_UsingUnityIOCContainer_With_Enterprise
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using Microsoft.Practices.Unity;

    public static class Program
    {
        public static void Main(string[] args)
        {
            var unity = new UnityContainer().AddNewExtension<EnterpriseLibraryCoreExtension>();

            // var entLogger = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
            var entLogger = unity.Resolve<LogWriter>();
            entLogger.Write("EL002_UsingUnityIOCContainer_With_Enterprise");
        }
    }
}
