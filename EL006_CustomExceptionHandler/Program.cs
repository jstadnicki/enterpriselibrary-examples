﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL006_CustomExceptionHandler
{
    using System.Collections.Specialized;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer().AddNewExtension<EnterpriseLibraryCoreExtension>();
            var exceptionManager = container.Resolve<ExceptionManager>();


            try
            {
                DoExceptionStuff();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Cought application level exception");
                Exception exceptionToRethrow = null;
                bool shouldRethrow = exceptionManager.HandleException(exception, "Policy", out exceptionToRethrow);
                if (shouldRethrow)
                {
                    throw exceptionToRethrow;
                }

                Console.WriteLine(exception.ToString());
                Console.ReadLine();
            }
        }

        private static void DoExceptionStuff()
        {
            Console.WriteLine("Wait for it...");
            throw new Exception("New exception with dangerous data inside");
            Console.WriteLine("And gone");
        }
    }

    [ConfigurationElementType(typeof(CustomHandlerData))]
    public class MyCustomExceptionHandler : IExceptionHandler
    {
        public MyCustomExceptionHandler(NameValueCollection collection)
        {
            
        }
        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            var newException = new Exception(exception.Message.Replace("dangerous", "SAFE"));
            return newException;
        }
    }
}
