﻿using System.Collections.Generic;

namespace EL019_AdvanceLoging
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // ExtendedLogInformation();
            // TracerExample();
            CustomListenerExample();
        }

        private static void CustomListenerExample()
        {
            
        }

        private static void TracerExample()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var loger = container.Resolve<LogWriter>();
            var tracer = container.Resolve<TraceManager>();

            using (tracer.StartTrace("Tracing"))
            {
                loger.Write("First entry", "demo");
                loger.Write("Second entry", "UI");
                loger.Write("Last entry", "General");
            }
        }

        private static void ExtendedLogInformation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var loger = container.Resolve<LogWriter>();
            var logEntry = new LogEntry { Message = "Detailed message", Categories = new List<string> { "General" } };
            var extendedInfo = new Dictionary<string, object>();

            var debug = new DebugInformationProvider();
            debug.PopulateDictionary(extendedInfo);

            var managedSecurity = new ManagedSecurityContextInformationProvider();
            managedSecurity.PopulateDictionary(extendedInfo);

            var unmanaged = new UnmanagedSecurityContextInformationProvider();
            unmanaged.PopulateDictionary(extendedInfo);

            var complus = new ComPlusInformationProvider();
            complus.PopulateDictionary(extendedInfo);

            extendedInfo.Add("My custom info", "Custom information");

            logEntry.ExtendedProperties = extendedInfo;
            loger.Write(logEntry);
        }
    }
}
