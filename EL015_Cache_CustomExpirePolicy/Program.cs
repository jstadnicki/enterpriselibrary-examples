﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL015_Cache_CustomExpirePolicy
{
    using System.IO;
    using System.Threading;

    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

    class Program
    {
        static void Main(string[] args)
        {
            var cache = EnterpriseLibraryContainer.Current.GetInstance<ICacheManager>();

            var countDownPolicy = new CountDownCustomExpiryPolicy();
            File.WriteAllText("cachefile.txt", "niespodzianka");

            cache.Add("key", "EL015_Cache_CustomExpirePolicy", CacheItemPriority.Normal, new CustomRefreshCacheItem(), countDownPolicy);

            Console.WriteLine("1st attempt");
            checkCachedData(cache);
            Console.WriteLine("Rerfesh should kick in already");
            checkCachedData(cache);


        }

        private static void checkCachedData(ICacheManager cache)
        {
            object data = null;
            do
            {
                data = cache.GetData("key");
                Console.WriteLine(DateTime.Now.Millisecond.ToString() + ":\t" + data);
                Thread.Sleep(1000);
            }
            while (data != null);
            Console.WriteLine("Cache expired!");
        }
    }

    public class CountDownCustomExpiryPolicy : ICacheItemExpiration
    {
        private int counter = 12;
        public bool HasExpired()
        {
            --counter;
            if (counter > 0)
            {
                Console.WriteLine("({0})Wait for it...", counter);
            }
            else
            {
                Console.WriteLine("And gone!");
            }

            return counter <=  0;
        }

        public void Notify()
        {
        }

        public void Initialize(CacheItem owningCacheItem)
        {
        }
    }

    public class CustomRefreshCacheItem : ICacheItemRefreshAction
    {
        public void Refresh(string removedKey, object expiredValue, CacheItemRemovedReason removalReason)
        {
            Console.WriteLine("refreshed!");
            var content = File.ReadAllText("cachefile.txt");
            var cache = EnterpriseLibraryContainer.Current.GetInstance<ICacheManager>();
            cache.Add("key", content, CacheItemPriority.Normal, this, new CountDownCustomExpiryPolicy());
        }
    }
}
