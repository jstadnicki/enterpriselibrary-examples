﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL022_SimpleValidation
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Validation;
    using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // SimpleEmptyValidation();
            // SimpleValidationWithAnnotation();
            // ConfigurationBasedValidation();
            // CollectionValidation();
            // CompositeValidationExamples();
            SelfValidationExample();

        }

        private static void SelfValidationExample()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person5>();

            var person1 = new Person5();
            var person2 = new Person5() { Age = 4 };

            Console.WriteLine("null");
            CheckValidationResult(validator.Validate(person1));

            Console.WriteLine("Age 4");
            CheckValidationResult(validator.Validate(person2));

        }

        private static void CompositeValidationExamples()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person4>();
            var person0 = new Person4();
            var person1 = new Person4 { Age = 1, FirstName = "Henry" };
            var person2 = new Person4 { Age = 1, FirstName = "Jimmy" };
            var person3 = new Person4 { Age = 1, FirstName = "T" };
            var person4 = new Person4 { Age = 1, FirstName = "J" };

            Console.WriteLine("null");
            CheckValidationResult(validator.Validate(person0));
            Console.WriteLine("Henry");
            CheckValidationResult(validator.Validate(person1));
            Console.WriteLine("Jimmy");
            CheckValidationResult(validator.Validate(person2));
            Console.WriteLine("T");
            CheckValidationResult(validator.Validate(person3));
            Console.WriteLine("J");
            CheckValidationResult(validator.Validate(person4));
        }

        private static void CollectionValidation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person3>();

            var person = new Person3(); //{ Age = 1, FirstName = "bobo", LastName = "aaaaaaaaaaaaaa"};
            var result = validator.Validate(person);

            var planet = new Planet();
            planet.People = new List<Person3>
                {
                    new Person3(),
                    new Person3 { Age = 3 },
                    new Person3 { Age = 1, FirstName = "J" },
                    new Person3 { FirstName = "Joe" },
                    new Person3 { Age = 4, FirstName = "Jimmy" }
                };

            var planetValidator = factory.CreateValidator<Planet>();
            var planetResult = planetValidator.Validate(planet);


            CheckValidationResult(result);
            CheckValidationResult(planetResult);

        }

        private static void CheckValidationResult(ValidationResults result)
        {
            if (result.IsValid == false)
            {
                foreach (var validationResult in result)
                {
                    PrintValidationResult(validationResult);
                }
            }
            else
            {
                Console.WriteLine("Person is valid");
            }
        }

        private static void PrintValidationResult(ValidationResult validationResult)
        {
            Console.WriteLine(
                "Key: {0}\nMessage: {1}\nNestedValidationResults: {2}\nTag: {3}\nTarget: {4}\nValidator: {5}",
                validationResult.Key,
                validationResult.Message,
                validationResult.NestedValidationResults,
                validationResult.Tag,
                validationResult.Target,
                validationResult.Validator);

            if (validationResult.NestedValidationResults != null && validationResult.NestedValidationResults.Count() > 0)
            {
                foreach (var nestedValidationResult in validationResult.NestedValidationResults)
                {
                    PrintValidationResult(nestedValidationResult);
                }
            }

            Console.WriteLine();
        }

        private static void ConfigurationBasedValidation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person3>();

            var person = new Person3(); //{ Age = 1, FirstName = "bobo", LastName = "aaaaaaaaaaaaaa"};

            var result = validator.Validate(person);

            if (result.IsValid == false)
            {
                foreach (var validationResult in result)
                {
                    Console.WriteLine(
                        "Key: {0}\nMessage: {1}\nNestedValidationResults: {2}\nTag: {3}\nTarget: {4}\nValidator: {5}",
                        validationResult.Key,
                        validationResult.Message,
                        validationResult.NestedValidationResults,
                        validationResult.Tag,
                        validationResult.Target,
                        validationResult.Validator);
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Person is valid");
            }

            Console.ReadKey();
        }

        private static void SimpleValidationWithAnnotation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person2>();

            var person = new Person2 { Age = 20, FirstName = "Joe", LastName = "Doe" };

            var result = validator.Validate(person);

            if (result.IsValid == false)
            {
                foreach (var validationResult in result)
                {
                    Console.WriteLine(
                        "Key: {0}\nMessage: {1}\nNestedValidationResults: {2}\nTag: {3}\nTarget: {4}\nValidator: {5}",
                        validationResult.Key,
                        validationResult.Message,
                        validationResult.NestedValidationResults,
                        validationResult.Tag,
                        validationResult.Target,
                        validationResult.Validator);
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Person2 is valid");
            }

            Console.ReadKey();
        }

        private static void SimpleEmptyValidation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person>();

            var person = new Person();

            var result = validator.Validate(person);

            if (result.IsValid == false)
            {
                foreach (var validationResult in result)
                {
                    Console.WriteLine(validationResult.Message);
                }
            }
            else
            {
                Console.WriteLine("Person is valid");
            }

            Console.ReadKey();
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }

    public class Person2
    {
        [NotNullValidator(MessageTemplate = "First name cannot be null.")]
        [StringLengthValidator(2, RangeBoundaryType.Inclusive, 50, RangeBoundaryType.Inclusive, MessageTemplate = "Name must be at least 2 character and no more than 50.")]
        public string FirstName { get; set; }

        [NotNullValidator(MessageTemplate = "Last name cannot be null.")]
        public string LastName { get; set; }

        [RangeValidator(1, RangeBoundaryType.Inclusive, 120, RangeBoundaryType.Inclusive, MessageTemplate = "Age must between 1 and 120.")]
        public int Age { get; set; }
    }

    public class Person3
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }

    public class Planet
    {
        [ObjectCollectionValidator(typeof(Person3))]
        public List<Person3> People { get; set; }
    }

    public class Person4
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }



    [HasSelfValidation]
    public class Person5
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        [SelfValidation]
        public void Validate(ValidationResults result)
        {
            if (this.Age * this.Age > 10)
            {
                result.AddResult(new ValidationResult("Age squared validation failed", this, "AgeCheck", string.Empty, null));
            }
        }
    }
}
