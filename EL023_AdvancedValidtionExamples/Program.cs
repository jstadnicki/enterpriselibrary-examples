﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL023_AdvancedValidtionExamples
{
    using System.ComponentModel.DataAnnotations;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Validation;
    using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
    using Microsoft.Practices.Unity;

    using ValidationResult = Microsoft.Practices.EnterpriseLibrary.Validation.ValidationResult;

    class Program
    {
        static void Main(string[] args)
        {
            // ValidationAttributesExamples();
            // UseOfMetadataClassAttribute();  // takie sztucznki mozna takze zastosowac przy wykorzystaniu klas czesciowych (partial), w takim przypadku klasa
            // docelowa nie musi w zaden sposob bym modyfikowana
            // UsageOfRuleSets();
            // Mozna zdecydowac sie na jedna z fabryk, albo z atrybutow albo z xaml
            // AttributeValidationFactory, ConfigurationVAlidationFactory, ...

        }

        private static void UsageOfRuleSets()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            Console.WriteLine("No ruleset was used - yet");

            {
                var factory = container.Resolve<ValidatorFactory>();
                var validator = factory.CreateValidator<Person3>();

                var person1 = new Person3();
                var person2 = new Person3 { FirstName = "j" };
                var person3 = new Person3 { FirstName = "jareczek" };
                Console.WriteLine("");
                CheckValidationResult(validator.Validate(person1));
                Console.WriteLine("j");
                CheckValidationResult(validator.Validate(person2));
                Console.WriteLine("jareczek");
                CheckValidationResult(validator.Validate(person3));
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Go for 'A'.");
            {
                var factory = container.Resolve<ValidatorFactory>();
                var validator = factory.CreateValidator<Person3>("A");

                var person1 = new Person3();
                var person2 = new Person3 { FirstName = "t" };
                var person3 = new Person3 { FirstName = "jareczek" };
                Console.WriteLine("");
                CheckValidationResult(validator.Validate(person1));
                Console.WriteLine("j");
                CheckValidationResult(validator.Validate(person2));
                Console.WriteLine("jareczek");
                CheckValidationResult(validator.Validate(person3));
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Go for 'B'");
            {
                var factory = container.Resolve<ValidatorFactory>();
                var validator = factory.CreateValidator<Person3>("B");

                var person1 = new Person3();
                var person2 = new Person3 { FirstName = "t" };
                var person3 = new Person3 { FirstName = "jareczek" };
                Console.WriteLine("");
                CheckValidationResult(validator.Validate(person1));
                Console.WriteLine("j");
                CheckValidationResult(validator.Validate(person2));
                Console.WriteLine("jareczek");
                CheckValidationResult(validator.Validate(person3));
            }
        }

        private static void UseOfMetadataClassAttribute()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person2>();

            var person1 = new Person2();
            var person2 = new Person2 { FirstName = "t" };
            CheckValidationResult(validator.Validate(person1));
            CheckValidationResult(validator.Validate(person2));
        }

        private static void ValidationAttributesExamples()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var factory = container.Resolve<ValidatorFactory>();
            var validator = factory.CreateValidator<Person>();

            var person1 = new Person();
            var person2 = new Person { FirstName = "t" };
            CheckValidationResult(validator.Validate(person1));
            CheckValidationResult(validator.Validate(person2));
        }

        private static void CheckValidationResult(ValidationResults result)
        {
            if (result.IsValid == false)
            {
                foreach (var validationResult in result)
                {
                    PrintValidationResult(validationResult);
                }
            }
            else
            {
                Console.WriteLine("Person is valid");
            }
        }

        private static void PrintValidationResult(ValidationResult validationResult)
        {
            Console.WriteLine(
                "Key: {0}\nMessage: {1}\nNestedValidationResults: {2}\nTag: {3}\nTarget: {4}\nValidator: {5}",
                validationResult.Key,
                validationResult.Message,
                validationResult.NestedValidationResults,
                validationResult.Tag,
                validationResult.Target,
                validationResult.Validator);

            if (validationResult.NestedValidationResults != null && validationResult.NestedValidationResults.Count() > 0)
            {
                foreach (var nestedValidationResult in validationResult.NestedValidationResults)
                {
                    PrintValidationResult(nestedValidationResult);
                }
            }

            Console.WriteLine();
        }
    }

    public class Person
    {
        [StringLength(50, ErrorMessage = "STring was too short", MinimumLength = 5)]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }
    }

    public partial class Person2
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }

    [MetadataType(typeof(Person2Meta))]
    public partial class Person2
    {
    }

    public class Person2Meta
    {
        [StringLengthValidator(5, 500, ErrorMessage = "String was an incorrect length!")]
        public object FirstName { get; set; }
    }


    [MetadataType(typeof(PersonMeta3))]
    public class Person3
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }
    }

    class PersonMeta3
    {
        [StringLengthValidator(5, 50, ErrorMessage = "String length was too short", Ruleset = "A")]
        [StringLengthValidator(1, 5, ErrorMessage = "String length was too long", Ruleset = "B")]
        public object FirstName { get; set; }
    }
}
