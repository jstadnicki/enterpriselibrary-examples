﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL005_WCF_ExceptionHandling_ConsoleClient
{
    using System.ServiceModel;

    using EL005_WCF_ExceptionHandling;

    using EL005_WCF_ExceptionHandling_ConsoleClient.ServiceReference1;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var service = new Service1Client();
                var result = service.GetData(667);

                Console.WriteLine("Through service: " + result);
                Console.ReadKey();
            }
            catch (FaultException<MyContractException> exception)
            {
                Console.WriteLine("Cought:" + exception.ToString());
                Console.WriteLine("ErrorId:" + exception.Detail.ErrorId);
                Console.WriteLine("Message:" + exception.Detail.Message);
                Console.ReadKey();
            }
        }
    }
}
