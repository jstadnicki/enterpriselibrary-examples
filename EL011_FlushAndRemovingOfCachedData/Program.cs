﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL011_FlushAndRemovingOfCachedData
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var cacheManager = container.Resolve<ICacheManager>();


            Console.WriteLine("Before caching");
            showCachedData(cacheManager);
            
            cacheManager.Add("key", "EL011_FlushAndRemovingOfCachedData");
            cacheManager.Add("key2", "Another cached data");
            Console.WriteLine("After caching");
            
            showCachedData(cacheManager);
            cacheManager.Remove("key");
            Console.WriteLine("[key] Cache removed");
            
            showCachedData(cacheManager);
            cacheManager.Flush();
            Console.WriteLine("Cache flushed");
            
            showCachedData(cacheManager);

        }

        private static void showCachedData(ICacheManager cacheManager)
        {
            var data = cacheManager.GetData("key");
            if (data != null)
            {
                Console.WriteLine("[key]"+data);
            }
            else
            {
                Console.WriteLine("[key]: Cache is empty");
            }

            var data2 = cacheManager.GetData("key2");
            if (data2 != null)
            {
                Console.WriteLine("[key2]"+data2);
            }
            else
            {
                Console.WriteLine("[key2]: Cache is empty");
            }
        }
    }
}
