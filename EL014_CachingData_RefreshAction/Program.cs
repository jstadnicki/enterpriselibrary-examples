﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL014_CachingData_RefreshAction
{
    using System.IO;
    using System.Threading;

    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var cache = EnterpriseLibraryContainer.Current.GetInstance<ICacheManager>();

            var filename = "refreshfile.txt";
            File.WriteAllText(filename, DateTime.Now.ToString());
            var fileDep = new FileDependency(filename);
            
            Console.WriteLine("Before updating cache:");
            displayCacheContent(cache);

            Console.WriteLine("Cache added");
            cache.Add("key", "Original cached text", CacheItemPriority.Normal, new FileRefreshAction(filename), fileDep);
            displayCacheContent(cache);

            Console.WriteLine("Introducing file modifications");
            File.WriteAllText(filename, DateTime.Now.ToString());

            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(50);
                displayCacheContent(cache);
            }
            


        }

        private static void displayCacheContent(ICacheManager cache)
        {
            Console.WriteLine("displayCacheContent: "+DateTime.Now.Millisecond.ToString());
            var data = cache.GetData("key");
            if (data != null)
            {
                Console.WriteLine(data);
            }
            else
            {
                Console.WriteLine("expired");
            }
        }
    }

    [Serializable]
    class FileRefreshAction : ICacheItemRefreshAction
    {
        private readonly string filename;

        public FileRefreshAction(string filename)
        {
            this.filename = filename;
        }

        public void Refresh(string removedKey, object expiredValue, CacheItemRemovedReason removalReason)
        {
            Console.WriteLine("Refresh: "+DateTime.Now.Millisecond.ToString());

            var c = EnterpriseLibraryContainer.Current.GetInstance<ICacheManager>();
            var fileContent = File.ReadAllText(this.filename);

            var expiration = new FileDependency(this.filename);

            c.Add("key", fileContent, CacheItemPriority.Normal, this, expiration);
        }
    }
}
