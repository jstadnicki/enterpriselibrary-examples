﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL010_DatabaseCachingOptions
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // Target framework set to full 4.0 framework
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var cacheManager = container.Resolve<ICacheManager>();

            cacheManager.Add("key", "EL010_DatabaseCachingOptions");

            var data = cacheManager.GetData("key");
            if (data != null)
            {
                Console.WriteLine(data);
            }
            else
            {
                Console.WriteLine("expired");
            }

            Console.ReadLine();

        }
    }
}
