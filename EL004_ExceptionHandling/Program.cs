﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL004_ExceptionHandling
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // Example_Of_Usege_Of_Process();
            foo();
        }

        static void foo()
        {
            try
            {
                DoHandleStuff();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception cought");
                Console.Write(exception.ToString());
                Console.ReadLine();
            }
        }

        private static void DoHandleStuff()
        {
            var container = new UnityContainer().AddNewExtension<EnterpriseLibraryCoreExtension>();
            var exceptionManager = container.Resolve<ExceptionManager>();

            try
            {
                MyExceptionalCode();
            }
            catch (Exception exception)
            {
                Exception newException;
                if (exceptionManager.HandleException(exception, "Policy", out newException))
                {
                    throw newException;
                }

                Console.WriteLine("Didn't rethrow");
            }
        }

        private static void Example_Of_Usege_Of_Process()
        {
            try
            {
                DoExampleStuff();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Cought exception");
                Console.WriteLine(exception.ToString());
                Console.ReadKey();
            }
        }

        private static void DoExampleStuff()
        {
            var container = new UnityContainer().AddNewExtension<EnterpriseLibraryCoreExtension>();
            var exceptionManager = container.Resolve<ExceptionManager>();

            exceptionManager.Process(MyExceptionalCode, "Policy");
        }

        private static void MyExceptionalCode()
        {
            Console.WriteLine("Throwing an exception");
            throw new Exception("My basic exception");
            Console.Write("Passed the excption");
        }
    }
}
