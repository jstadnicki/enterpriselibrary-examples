﻿namespace EL016_DataAccess_SimpleExample
{
    using System;
    using System.Data;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using Microsoft.Practices.Unity;

    internal class Program
    {
        private static void Main(string[] args)
        {
            // ExecuteCommand();
            // ExecuteStoredProcedure();
            // ExecuteStoredProcedureWithParameter();
            // ExecuteQueryWithStoredParameter();
            // ExecuteScalarOperation();
            // NonQueryExample();
            UsingTransaction();

        }

        private static void UsingTransaction()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            try
            {
                using (var connection = db.CreateConnection())
                {
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        db.ExecuteReader(
                            CommandType.Text, "UPDATE fakenames SET givenname = 'jareczek' WHERE givnenames LIKE 'jarek%'");
                        transaction.Commit();
                        Console.WriteLine("UPDATED OK!");
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                        transaction.Rollback();
                        Console.WriteLine("TRANSACTION rollbacked!");
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("John Sonmez to pipa!");
                Console.WriteLine(exception);
            }

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        private static void NonQueryExample()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            var statement = "UPDATE fakenames SET givenname = 'jarek' WHERE givenname LIKE 'jar%'";

            var count = db.ExecuteNonQuery(CommandType.Text, statement);
            Console.WriteLine("Updated: {0}", count);
        }

        private static void ExecuteScalarOperation()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            var value = db.ExecuteScalar(CommandType.Text, "SELECT COUNT(*) FROM fakenames");
            Console.WriteLine("Count: {0}", value.ToString());

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        private static void ExecuteQueryWithStoredParameter()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            var statement = "SELECT * FROM fakenames WHERE city = @cityname";
            using (var command = db.GetSqlStringCommand(statement))
            {
                db.AddInParameter(command, "cityname", DbType.AnsiStringFixedLength, "omaha");
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var s = string.Format(
                            "{0}\t{1}\t\t{2}",
                            reader.GetString(reader.GetOrdinal("givenname")),
                            reader.GetString(reader.GetOrdinal("surname")),
                            reader.GetString(reader.GetOrdinal("city")));
                        Console.WriteLine(s);
                    }
                }
            }



            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        private static void ExecuteStoredProcedureWithParameter()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            using (var reader = db.ExecuteReader("Top10SurnamesWithGivenNameLike", new object[] { "john" }))
            {
                while (reader.Read())
                {
                    Console.WriteLine("Top10LastNames:\t" + reader.GetString(reader.GetOrdinal("surname")));
                }
            }

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        private static void ExecuteStoredProcedure()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            using (var reader = db.ExecuteReader(CommandType.StoredProcedure, "Top10LastNames"))
            {
                while (reader.Read())
                {
                    Console.WriteLine("Top10LastNames:\t" + reader.GetString(reader.GetOrdinal("S")));
                }
            }

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        private static void ExecuteCommand()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            using (var reader = db.ExecuteReader(CommandType.Text, "SELECT * FROM fakenames"))
            {
                while (reader.Read())
                {
                    Console.WriteLine("SURNAME:\t" + reader.GetString(reader.GetOrdinal("surname")));
                }
            }

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }
    }
}