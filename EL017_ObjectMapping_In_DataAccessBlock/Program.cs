﻿using System;

namespace EL017_ObjectMapping_In_DataAccessBlock
{
    using System.Text;
    using System.Threading;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // ReadFakePeople();
            // ReadSiplePerson();
            // GetXMLResults();
            WorkAsynchronous();
        }

        private static void WorkAsynchronous()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>() as SqlDatabase;

            var statement1 = "SELECT TOP 5 givenname FROM fakenames";
            var statement2 = "SELECT TOP 5 surname FROM fakenames";

            var cmd1 = db.GetSqlStringCommand(statement1);
            var cmd2 = db.GetSqlStringCommand(statement2);

            db.BeginExecuteReader(
                cmd1,
                ar =>
                {
                    using (var reader = db.EndExecuteReader(ar))
                    {
                        while (reader.Read())
                        {
                            Thread.Sleep(50); // similate delay
                            Console.WriteLine("First name:\t{0}", reader.GetString(reader.GetOrdinal("givenname")));
                        }
                    }
                },
                null);

            db.BeginExecuteReader(
                cmd2,
                ar =>
                {
                    using (var reader = db.EndExecuteReader(ar))
                    {
                        while (reader.Read())
                        {
                            Thread.Sleep(50); // similate delay
                            Console.WriteLine("Last name:\t{0}", reader.GetString(reader.GetOrdinal("surname")));
                        }
                    }
                },
                null);

            Console.WriteLine("All commands executed, waiting for results.");
            Console.ReadKey();
        }

        private static void GetXMLResults()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>() as SqlDatabase;

            var statement = "SELECT * FROM fakenames FOR XML AUTO";
            using (var command = db.GetSqlStringCommand(statement))
            {
                using (var xmlreader = db.ExecuteXmlReader(command))
                {
                    while (!xmlreader.EOF)
                    {
                        if (xmlreader.IsStartElement())
                        {
                            Console.WriteLine(xmlreader.ReadOuterXml());
                        }
                    }
                }
            }
        }

        private static void ReadSiplePerson()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            var mapper = MapBuilder<SimpleFakePerson>.MapAllProperties().Map(x => x.FullName).WithFunc(
                x =>
                {
                    var sb = new StringBuilder();
                    sb.Append(x.GetString(x.GetOrdinal("givenname")));
                    sb.Append(x.GetString(x.GetOrdinal("surname")));
                    return sb.ToString();
                }).Build();

            var people = db.ExecuteSqlStringAccessor<SimpleFakePerson>("SELECT * FROM fakenames", mapper);

            foreach (var fakePerson in people)
            {
                Console.WriteLine(fakePerson);
            }
        }

        private static void ReadFakePeople()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var db = container.Resolve<Database>();

            var people = db.ExecuteSqlStringAccessor<FakePerson>("SELECT * FROM fakenames");

            foreach (var fakePerson in people)
            {
                Console.WriteLine(fakePerson);
            }
        }
    }

    public class FakePerson
    {
        public string givenname { get; set; }
        public string surname { get; set; }
        public string city { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(this.givenname + "\t");
            sb.Append(this.surname + "\t");
            sb.Append("from: \t" + this.city + "\t");

            return sb.ToString();
        }
    }

    public class SimpleFakePerson
    {
        public string FullName { get; set; }

        public string City { get; set; }

        public override string ToString()
        {
            return string.Format("Fullname:\t{0},\n\tLives in {1}", this.FullName, this.City);
        }
    }
}
