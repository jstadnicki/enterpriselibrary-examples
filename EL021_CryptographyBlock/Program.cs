﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL021_CryptographyBlock
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // BasicEncryptionDecryption();
            // ObjectEncryptionDecryption();
            HashingDemo();
        }

        private static void HashingDemo()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var manager = container.Resolve<CryptographyManager>();

            string text = "Helo hashed world!";
            var hashed = manager.CreateHash("SHA512Managed", text);
            Console.WriteLine(text);
            Console.WriteLine(hashed);
        }

        private static void ObjectEncryptionDecryption()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var manager = container.Resolve<CryptographyManager>();
            var person = new Person { Name = "jareczek", Age = 21 };
            byte[] bytes = PersonToBytes(person);

            var e = manager.EncryptSymmetric("RijndaelManaged", bytes);
            Console.WriteLine("Serialized and encrupted: {0}", e);
            var d = BytesToObject(
                manager.DecryptSymmetric("RijndaelManaged", e)) as Person;
            Console.WriteLine("Serialized and encrupted: {0}", d.ToString());

        }

        private static byte[] PersonToBytes(Person person)
        {
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, person);
            return ms.GetBuffer();
        }

        public static object BytesToObject(byte[] data)
        {
            var ms = new MemoryStream(data);
            var bf = new BinaryFormatter();
            return bf.Deserialize(ms);
        }

        private static void BasicEncryptionDecryption()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var manager = container.Resolve<CryptographyManager>();

            string message = "Hello World";
            Console.WriteLine(message);

            var encrypted = manager.EncryptSymmetric("RijndaelManaged", message);
            Console.WriteLine(encrypted);

            var decrypted = manager.DecryptSymmetric("RijndaelManaged", encrypted);

            Console.WriteLine(decrypted);

            Console.ReadKey();
        }
    }

    [Serializable]
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
