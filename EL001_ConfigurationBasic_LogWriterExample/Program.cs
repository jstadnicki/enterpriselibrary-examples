﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL001_ConfigurationBasic_LogWriterExample
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Logging;

    class Program
    {
        static void Main(string[] args)
        {
            // In order to change the output genereated by LogWriter
            // Right click on app.config choose orange edit configuration file
            // In formater update the output information you want to have, save the app.config, rerun the application
            // TODO how to write to the same file?
            var writer = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
            writer.Write("This is a general log message", "General");
            writer.Write("They are real", "Unicorn");
        }
    }
}
