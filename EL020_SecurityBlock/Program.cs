﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL020_SecurityBlock
{
    using System.Security.Principal;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Security;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {

            // Dodatkowo dane można zapisywać na dysku, poszukaj opcji backing store w cache settings.

            // CachingAIdentity();
            // ReadingWindowsIdentityFromELToken();
            // UsingCacheWithWindowsGenericPrinciple();
            // ExpiringCacheCredentials();
            // DefaultAuthorizationProvider();

            UsingAzManProvider();
        }

        private static void UsingAzManProvider()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();
            var auth = container.Resolve<IAuthorizationProvider>();

            var identity = WindowsIdentity.GetCurrent();
            
            var principle = new GenericPrincipal(identity, new string[] { "Sales", "Marketer" }); // use Sales

            Console.WriteLine("Is sales:{0}", principle.IsInRole("Sales"));
            Console.WriteLine("Is Marketer:{0}", principle.IsInRole("Marketer"));
            Console.WriteLine("Is CEO:{0}", principle.IsInRole("CEO"));

            var token = cache.SavePrincipal(principle);
            principle = null;
            principle = cache.GetPrincipal(token) as GenericPrincipal;

            // Aby zmienić zachowanie tego auth należy dodać przy pomocy azman.msc
            // Lokalnego użytkownika do roli Sales i ponownie uruchomić aplikację, ponowna kompilacja nie będzie potrzbna.
            // Należy także pamiętać, że teraz domyślnym prowyderem autoryzacji jest azman.
            Console.WriteLine(
                "Can read sales report (using IAuthorizationProvide): {0}",
                auth.Authorize(principle, "Can read sales report"));
        }

        private static void DefaultAuthorizationProvider()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();
            var auth = container.Resolve<IAuthorizationProvider>();

            var identity = WindowsIdentity.GetCurrent();
            var principle = new GenericPrincipal(identity, new string[] { "Admin", "Marketer" }); // use Sales

            Console.WriteLine("Is sales:{0}", principle.IsInRole("Sales"));
            Console.WriteLine("Is Marketer:{0}", principle.IsInRole("Marketer"));
            Console.WriteLine("Is CEO:{0}", principle.IsInRole("CEO"));

            var token = cache.SavePrincipal(principle);
            principle = null;
            principle = cache.GetPrincipal(token) as GenericPrincipal;

            Console.WriteLine(
                "Can read sales report (using IAuthorizationProvide): {0}",
                auth.Authorize(principle, "Can read sales report"));

        }

        private static void ExpiringCacheCredentials()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();

            var identity = WindowsIdentity.GetCurrent();

            Console.WriteLine(identity.Name);
            Console.WriteLine(identity.IsAuthenticated);
            var token = cache.SaveIdentity(identity);
            Console.WriteLine("Token: " + token.Value);

            Console.WriteLine("Now clearing the identity, and trying to read it using token");
            Console.WriteLine("In addition expiring the token.");
            identity = null;

            // The same can be done with principal
            cache.ExpireIdentity(token);
            
            identity = cache.GetIdentity(token) as WindowsIdentity;

            try
            {
                Console.WriteLine(identity.Name);
                Console.WriteLine(identity.IsAuthenticated);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadLine();
        }

        private static void UsingCacheWithWindowsGenericPrinciple()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();

            var identity = WindowsIdentity.GetCurrent();
            var principle = new GenericPrincipal(identity, new string[] { "Sales", "Marketer" });
            
            Console.WriteLine("Is sales:{0}", principle.IsInRole("Sales"));
            Console.WriteLine("Is Marketer:{0}", principle.IsInRole("Marketer"));
            Console.WriteLine("Is CEO:{0}", principle.IsInRole("CEO"));

            var token = cache.SavePrincipal(principle);
            principle = null;
            principle = cache.GetPrincipal(token) as GenericPrincipal;

            Console.WriteLine("After principle was null and read from cache.");

            Console.WriteLine("Is sales:{0}", principle.IsInRole("Sales"));
            Console.WriteLine("Is Marketer:{0}", principle.IsInRole("Marketer"));
            Console.WriteLine("Is CEO:{0}", principle.IsInRole("CEO"));



        }

        private static void ReadingWindowsIdentityFromELToken()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();

            var identity = WindowsIdentity.GetCurrent();

            Console.WriteLine(identity.Name);
            Console.WriteLine(identity.IsAuthenticated);
            var token = cache.SaveIdentity(identity);
            Console.WriteLine("Token: " + token.Value);

            Console.WriteLine("Now clearing the identity, and trying to read it using token");
            identity = null;
            identity = cache.GetIdentity(token) as WindowsIdentity;

            Console.WriteLine(identity.Name);
            Console.WriteLine(identity.IsAuthenticated);

            Console.ReadLine();
        }

        private static void CachingAIdentity()
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cache = container.Resolve<ISecurityCacheProvider>();

            var identity = WindowsIdentity.GetCurrent();

            Console.WriteLine(identity.Name);
            Console.WriteLine(identity.IsAuthenticated);

            var token = cache.SaveIdentity(identity);
            Console.WriteLine("Token: " + token.Value);

            Console.ReadLine();
        }
    }
}
