﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL013_CacheExpiration_UsingExtended_FormatTime
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var cache = container.Resolve<ICacheManager>();

            // * * * * *
            // minute hour day month week_day
            // * * * * * every minute
            // 1 * * * * every first minute
            // * 2 * * * every second hours
            // * * 3 * * every third day
            // * * * 4 * every fourth months
            // * * * * 1 every SECOND day of the week (culture dependend? => Is Monday or Sunday a first day of the week?)
            // 10 2 * * * every 10 minute of second hour

            var timeExpiration = new ExtendedFormatTime("10 12 * * 7");
            cache.Add("key", "EL013_CacheExpiration_UsingExtended_FormatTime", CacheItemPriority.Normal, null, timeExpiration);


        }
    }
}
