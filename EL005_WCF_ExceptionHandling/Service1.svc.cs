﻿namespace EL005_WCF_ExceptionHandling
{
    using System;

    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;

    [ExceptionShielding("Policy")]
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            throw new Exception("Service broken");
            return string.Format("You entered: {0}", value);
        }
    }
}
