﻿using System.ServiceModel;

namespace EL005_WCF_ExceptionHandling
{
    using System;
    using System.Runtime.Serialization;

    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [FaultContract(typeof(MyContractException))]
        string GetData(int value);
    }

    [DataContract]
    public class MyContractException
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public Guid ErrorId { get; set; }
    }
}
