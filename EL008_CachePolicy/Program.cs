﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL008_CachePolicy
{
    using System.Threading;

    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cacheManager = container.Resolve<ICacheManager>();

            var absoluteTime = new AbsoluteTime(TimeSpan.FromSeconds(2));

            cacheManager.Add("keyCache", "AbsoluteTime 3s", CacheItemPriority.Normal, null, absoluteTime);

            checkTimeExpireCache(cacheManager);

            var slidingTime = new SlidingTime(TimeSpan.FromSeconds(2));
            cacheManager.Add("keyCache", "SlidingTime 3s", CacheItemPriority.Normal, null, slidingTime);

            checkSlidingExpireCache(cacheManager);

            var absoluteTime2 = new AbsoluteTime(DateTime.Now.AddSeconds(2));
            cacheManager.Add("keyCache", DateTime.Now.AddSeconds(2).ToString(), CacheItemPriority.Normal, null, absoluteTime2);
            checkTimeExpireCache(cacheManager);



        }

        private static void checkSlidingExpireCache(ICacheManager cacheManager)
        {
            Console.WriteLine("Slide time expiration demo:\n\n");

            object data = null;
            int sleepTime = 0;
            do
            {
                data = cacheManager.GetData("keyCache");
                Console.WriteLine(DateTime.Now.ToLongTimeString() + "\t" + data);
                Console.WriteLine("Sleep for: {0}[ms]", sleepTime);
                sleepTime += 333;
                Thread.Sleep(sleepTime);

            }
            while (data != null);

            Console.WriteLine("Cache expired!");

        }

        private static void checkTimeExpireCache(ICacheManager cacheManager)
        {
            Console.WriteLine("Cache expiration demo:\n\n");
            object data = null;
            do
            {
                data = cacheManager.GetData("keyCache");
                Console.WriteLine(DateTime.Now.ToLongTimeString() + "\t" + data);
                Thread.Sleep(333);
            }
            while (data != null);
            Console.WriteLine("Cache expired!");
        }
    }
}
