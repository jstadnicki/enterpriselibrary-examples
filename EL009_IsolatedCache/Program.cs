﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL009_IsolatedCache
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            // data will be saved into this folder:
            // c:\Users\Jarosław\AppData\Local\IsolatedStorage\
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var cacheManager = container.Resolve<ICacheManager>();

            cacheManager.Add("demo", "demo data");
            
            var slidingExpiration = new SlidingTime(TimeSpan.FromSeconds(1000));

            cacheManager.Add("demo2", "other data", CacheItemPriority.NotRemovable, null, slidingExpiration);

            var data = cacheManager.GetData("demo2");

            if (data != null)
            {
                Console.WriteLine(data);
            }
            else
            {
                Console.WriteLine("expired");
            }

            Console.ReadLine();
        }
    }
}
