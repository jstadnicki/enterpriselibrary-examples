﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL007_CachingExample
{
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();

            var cacheManager = container.Resolve<ICacheManager>();

            Console.WriteLine("Before inserting data");

            ReadCacheData(cacheManager);
            cacheManager.Add("dummyData", "Demo cached data");

            Console.WriteLine("After inserting data");
            ReadCacheData(cacheManager);

            Console.ReadLine();
        }

        private static void ReadCacheData(ICacheManager cacheManager)
        {
            // var cachedvalue = cacheManager.Contains("dummyData");
            var cachedValue = cacheManager.GetData("dummyData");
            if (cachedValue != null)
            {
                Console.WriteLine(cachedValue);
            }
            else
            {
                Console.WriteLine("No cached data found!");
            }
        }
    }
}
