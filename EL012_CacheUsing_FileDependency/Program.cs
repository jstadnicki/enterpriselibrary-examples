﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL012_CacheUsing_FileDependency
{
    using System.IO;

    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.Unity;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var cache = container.Resolve<ICacheManager>();

            Console.WriteLine("Before added into cache");
            displayCacheContent(cache);
            Console.WriteLine();
            cache.Add("key", "from code: EL012_CacheUsing_FileDependency");
            Console.WriteLine();

            string filename = "demofile.txt";
            File.WriteAllText(filename, "EL012_CacheUsing_FileDependency: file content");
            var filedep = new FileDependency(filename);
            
            Console.WriteLine();
            cache.Add("key2", "cached2", CacheItemPriority.Normal, null, filedep);
            Console.WriteLine("Afeter added into cache, file dependency introduced. File exists.");
            displayCacheContent(cache);
            
            Console.WriteLine();
            Console.WriteLine("dependency file updated with new text");
            File.WriteAllText(filename, "blah");
            displayCacheContent(cache);
            
            Console.WriteLine();
            File.Delete(filename);
            Console.WriteLine("Dependency file deleted");
            displayCacheContent(cache);
        }

        private static void displayCacheContent(ICacheManager cache)
        {
            object data = null;
            data = cache.GetData("key");
            if (data != null)
            {
                Console.WriteLine("key:" + data);
            }
            else
            {
                Console.WriteLine("key expired");
            }

            data = cache.GetData("key2");
            if (data != null)
            {
                Console.WriteLine("key2:" + data);
            }
            else
            {
                Console.WriteLine("key2 expired");
            }
        }
    }
}
