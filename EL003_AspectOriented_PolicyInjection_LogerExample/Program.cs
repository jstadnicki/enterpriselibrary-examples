﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL003_AspectOriented_PolicyInjection_LogerExample
{
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer().AddNewExtension<EnterpriseLibraryCoreExtension>();
            container.AddNewExtension<Interception>();

            container.RegisterType<ICat, Cat>(
                new InterceptionBehavior<PolicyInjectionBehavior>(), new Interceptor<TransparentProxyInterceptor>());

            container.Configure<Interception>()
                .AddPolicy("log-meows")
                .AddMatchingRule<MemberNameMatchingRule>(new InjectionConstructor("Meow", true))
                .AddCallHandler<LogCallHandler>(
                    new ContainerControlledLifetimeManager(),
                    new InjectionConstructor(
                        typeof(LogWriter), 9000, true, true, "Doing a meow", "And done", true, true, true, 1, 0));



            var cat = container.Resolve<ICat>();
            cat.Meow();

        }
    }

    internal class Cat : ICat
    {
        private readonly LogWriter writer;

        public Cat(LogWriter writer)
        {
            this.writer = writer;
        }

        public void Meow()
        {
            this.writer.Write("Meowwww");
        }
    }

    internal interface ICat
    {
        void Meow();
    }
}
