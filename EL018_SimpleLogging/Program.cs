﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EL018_SimpleLogging
{
    using System.Diagnostics;

    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using Microsoft.Practices.Unity;

    class Program
    {

        private static void Main(string[] args)
        {
            // SimpleLogExample();
            // LogWithCategories();
            // LogWithLogEntry();
            // FilterLoging_CheckingIfLoggingIsEnabled();
            // FilteringLogs();
            // PriorityFilterLogs();
        }

        private static void PriorityFilterLogs()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var loger = container.Resolve<LogWriter>();

            var entry0 = new LogEntry
            {
                Message = "Logging with priority 0",
                Priority = 0,
            };

            var entry_minus_1 = new LogEntry // still will be loged in
            {
                Message = "Logging with priority -1",
                Priority = -1,
            };

            var entry100 = new LogEntry
            {
                Message = "Logging with priority 100",
                Priority = 100,
            };

            var entry101 = new LogEntry  // will be droped out
            {
                Message = "Logging with priority 101",
                Priority = 101,
            };

            loger.Write(entry0);
            loger.Write(entry_minus_1);
            loger.Write(entry100);
            loger.Write(entry101);
        }

        private static void FilteringLogs()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var loger = container.Resolve<LogWriter>();

            loger.Write("UI message", "UI");
            loger.Write("Demo message", "Demo");       // this one is denied from loging
            loger.Write("Data message", "Data");
            loger.Write("Log message", "Log");
        }

        private static void FilterLoging_CheckingIfLoggingIsEnabled()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var loger = container.Resolve<LogWriter>();

            if (loger.IsLoggingEnabled())
            {
                loger.Write("log message", "Demo");
            }

        }

        private static void LogWithLogEntry()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var loger = container.Resolve<LogWriter>();

            var entry = new LogEntry
                {
                    Message = "Logging with logentry",
                    Categories = new List<string> { "Debug", "UI" },
                    Priority = 3,
                    Title = "Log title",
                    EventId = 12344
                };

            loger.Write(entry);

        }

        private static void LogWithCategories()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();
            var loger = container.Resolve<LogWriter>();

            loger.Write("EL018_SimpleLogging", "Debug", 1, 1234, TraceEventType.Error, "log message");
        }

        private static void SimpleLogExample()
        {
            IUnityContainer container = new UnityContainer();
            container.AddNewExtension<EnterpriseLibraryCoreExtension>();


            var loger = container.Resolve<LogWriter>();
            loger.Write("EL018_SimpleLogging");
        }
    }
}
